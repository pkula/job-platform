import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { noop } from 'rxjs';
import { AppState } from '../../reducers';
import { login, reload } from '../auth.actions';
import { AuthService } from '../auth.service';
import {take, tap} from 'rxjs/operators';

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  form: UntypedFormGroup;

  constructor(
      private fb:UntypedFormBuilder,
      private auth: AuthService,
      private router:Router,
      private store: Store<AppState>) {

      this.form = fb.group({
        firstName: ['', [Validators.required]],
        lastName: ['', [Validators.required]],
        email: ['', [Validators.required]],
        password: ['', [Validators.required]],
        role: ['', [Validators.required]]
      });

  }

  ngOnInit() {

  }

  register() {

      const val = this.form.value;
      val.favouriteJobs=[]
      val.appliedJobs=[]
      val.createdJobs=[]
      console.log(val);
   

      this.auth.register(val)
          .pipe(take(1),
              tap(user => {

                  console.log(user);

                  this.store.dispatch(login({user}));

                  this.router.navigateByUrl('/jobs');

              })
          )
          .subscribe(
              noop,
              () => alert('Login Failed')
          );

  }

}
