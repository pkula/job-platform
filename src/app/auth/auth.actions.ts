import {createAction, props} from '@ngrx/store';
import {User} from './model/user.model';


export const login = createAction(
    "[Login Page] User Login",
    props<{user: User}>()
);

export const profile = createAction(
  "[Profile Page] User Profile",
  props<{user: User}>()
)

export const reload  =createAction(
  "[Profile Reload] Reload User Profile",
  props<{user: User}>()
)

export const logout = createAction(
  "[Top Menu] Logout"
);
