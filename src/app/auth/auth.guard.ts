import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {AppState} from '../reducers';
import {select, Store} from '@ngrx/store';
import {isLoggedIn, selectAuthState, loggedUser} from './auth.selectors';
import {map, tap} from 'rxjs/operators';
import {login, logout} from './auth.actions';


@Injectable()
export class AuthGuard implements CanActivate {

    constructor(
        private store: Store<AppState>,
        private router: Router) {

    }
 
    private handleLoggedUserObs(rolePermision: (string | undefined)[]): Observable<boolean> {
      
        return this.store.pipe(
            select(loggedUser),
            map((user: any) => rolePermision.includes(user.role))
         );
    }
        
    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> {
            let rolePermision: (string | undefined)[] = route.data.rolePermision;
           return this.handleLoggedUserObs(rolePermision).pipe(
                        tap(x => {
                            if (!x) {
                                this.router.navigateByUrl('/login');
                            }
                        })
        )
          
       

    }

}
