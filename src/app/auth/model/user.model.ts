

export interface User {
    firstName: string;
    lastName: string;
    role: string;
    id: string;
    email: string;
    favouriteJobs: number[];
    appliedJobs: number[];
    createdJobs: number[];
}

