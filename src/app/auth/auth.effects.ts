import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {AuthActions} from './action-types';
import {map, tap} from 'rxjs/operators';
import {Router} from '@angular/router';


@Injectable()
export class AuthEffects {

    login$ = createEffect(() =>
        this.actions$
            .pipe(
                ofType(AuthActions.login),
                map(action =>{
                    localStorage.setItem('user',
                        JSON.stringify(action.user))
                        return AuthActions.profile({user: action.user})
                    }
                )
            )
    ,
    {dispatch: false});

    reload$ = createEffect(() =>
        this.actions$
            .pipe(
                ofType(AuthActions.reload),
                tap(action =>{
                    localStorage.setItem('user',
                        JSON.stringify(action.user))
                    }
                )
            )
    ,
    {dispatch: false});

    logout$ = createEffect(() =>
        this.actions$
            .pipe(
                ofType(AuthActions.logout),
                tap(action => {
                    localStorage.removeItem('user');
                    this.router.navigateByUrl('/login').then(() => {
                        window.location.reload();
                      });;
                })
            )
    , {dispatch: false});


    constructor(private actions$: Actions,
                private router: Router) {

    }

}
