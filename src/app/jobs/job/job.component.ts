import { AfterViewInit, ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Job } from '../model/job';
import { Observable, of } from 'rxjs';
import { concatMap, delay, filter, first, map, shareReplay, tap, withLatestFrom } from 'rxjs/operators';
import { JobsHttpService } from '../services/jobs-http.service';
import { JobEntityService } from '../services/job-entity.service';
import { User } from '../../auth/model/user.model';
import { select, Store } from '@ngrx/store';
import {isLoggedIn, selectAuthState, loggedUser} from '../../auth/auth.selectors';


@Component({
    selector: 'job',
    templateUrl: './job.component.html',
    styleUrls: ['./job.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class JobComponent implements OnInit {

    job$: Observable<Job>;

    loading$: Observable<boolean>;

    applicants$: Observable<User[]>;

    displayedColumns = ['email', 'firstName', 'lastName'];

    nextPage = 0;
    showApplicantsTable = false;

    constructor(
        private jobsService: JobEntityService,
        private route: ActivatedRoute,
        private store: Store) {

    }

    ngOnInit() {
        this.store
            .pipe(
                select(loggedUser)
            ).subscribe(user => {
                this.showApplicantsTable = user.role == 'recruiter';
            });
        const jobUrl = this.route.snapshot.paramMap.get('jobUrl');

        this.job$ = this.jobsService.entities$
            .pipe(
                map(jobs => jobs.find(job => job.url == jobUrl))
            );

        this.applicants$ = this.job$.pipe(map(it => it.applicants))


    }



}
