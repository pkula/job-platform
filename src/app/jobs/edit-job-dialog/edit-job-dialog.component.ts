import {ChangeDetectionStrategy, Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Job} from '../model/job';
import {UntypedFormBuilder, UntypedFormGroup, Validators} from '@angular/forms';
import {Observable,} from 'rxjs';
import {JobEntityService} from '../services/job-entity.service';

@Component({
    selector: 'job-dialog',
    templateUrl: './edit-job-dialog.component.html',
    styleUrls: ['./edit-job-dialog.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditJobDialogComponent {

    form: UntypedFormGroup;

    dialogTitle: string;

    job: Job;

    mode: 'create' | 'update';

    loading$: Observable<boolean>;

    constructor(
        private fb: UntypedFormBuilder,
        private dialogRef: MatDialogRef<EditJobDialogComponent>,
        @Inject(MAT_DIALOG_DATA) data,
        private jobsService: JobEntityService) {

        this.dialogTitle = data.dialogTitle;
        this.job = data.job;
        this.mode = data.mode;

        const formControls = {
            description: ['', Validators.required],
            category: ['', Validators.required],
            longDescription: ['', Validators.required]
        };

        if (this.mode == 'update') {
            this.form = this.fb.group(formControls);
            this.form.patchValue({...data.job});
        } else if (this.mode == 'create') {
            this.form = this.fb.group({
                ...formControls,
                url: ['', Validators.required],
                iconUrl: ['', Validators.required]
            });
        }
    }

    onClose() {
        this.dialogRef.close();
    }

    onSave() {

        const job: Job = {
            ...this.job,
            ...this.form.value
        };

        if (this.mode == 'update') {

            this.jobsService.update(job);

            this.dialogRef.close();
        } else if (this.mode == 'create') {
            job.applicants=[]
            this.jobsService.add(job)
                .subscribe(
                    newJob => {

                        console.log('New Job', newJob);

                        this.dialogRef.close();

                    }
                );

        }


    }


}
