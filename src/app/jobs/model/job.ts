import { User } from "../../auth/model/user.model";

export interface Job {
  id: number;
  seqNo:number;
  url:string;
  iconUrl: string;
  jobListIcon: string;
  description: string;
  longDescription?: string;
  createdBy: number;
  category: string;
  applicants: User[];
}


export function compareJobs(c1:Job, c2: Job) {

  const compare = c1.seqNo - c2.seqNo;

  if (compare > 0) {
    return 1;
  }
  else if ( compare < 0) {
    return -1;
  }
  else return 0;

}
