import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeComponent} from './home/home.component';
import {JobsCardListComponent} from '../shared/jobs-card-list/jobs-card-list.component';
import {EditJobDialogComponent} from './edit-job-dialog/edit-job-dialog.component';
import {JobsHttpService} from './services/jobs-http.service';
import {JobComponent} from './job/job.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatDialogModule} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material/input';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatSelectModule} from '@angular/material/select';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatSortModule} from '@angular/material/sort';
import {MatTableModule} from '@angular/material/table';
import {MatTabsModule} from '@angular/material/tabs';
import {ReactiveFormsModule} from '@angular/forms';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {RouterModule, Routes} from '@angular/router';
import {EntityDataService, EntityDefinitionService, EntityMetadataMap} from '@ngrx/data';
import {compareJobs, Job} from './model/job';

import {JobEntityService} from './services/job-entity.service';
import {JobsResolver} from './services/jobs.resolver';
import {JobsDataService} from './services/jobs-data.service';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SharedModule } from '../shared/shared.module';
import {EffectsModule} from '@ngrx/effects';
import {AuthEffects} from '../auth/auth.effects';
import {JobEffects} from './job.effects';
import {MatChipsModule} from '@angular/material/chips';

export const jobsRoutes: Routes = [
    {
        path: '',
        component: HomeComponent,
        resolve: {
           jobs: JobsResolver
        }
    },
    {
        path: ':jobUrl',
        component: JobComponent,
        resolve: {
           jobs: JobsResolver
        }
    }
];

const entityMetadata: EntityMetadataMap = {
    Job: {
        sortComparer: compareJobs,
        entityDispatcherOptions: {
            optimisticUpdate: true
        }
    }
};


@NgModule({
    imports: [
        CommonModule,
        MatButtonModule,
        MatIconModule,
        MatCardModule,
        MatTabsModule,
        MatInputModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatProgressSpinnerModule,
        MatSlideToggleModule,
        MatDialogModule,
        MatSelectModule,
        MatDatepickerModule,
        MatMomentDateModule,
        FlexLayoutModule,
        ReactiveFormsModule,
        MatChipsModule,
        SharedModule,
        RouterModule.forChild(jobsRoutes),
        EffectsModule.forFeature([JobEffects])
    ],
    declarations: [
        HomeComponent,
        EditJobDialogComponent,
        JobComponent
    ],
    exports: [
        HomeComponent,
        EditJobDialogComponent,
        JobComponent
    ],
    providers: [
        JobsHttpService,
        JobsResolver,
        JobsDataService
    ]
})
export class JobsModule {

    constructor(
        private eds: EntityDefinitionService,
        private entityDataService: EntityDataService,
        private jobsDataService: JobsDataService) {

        eds.registerMetadataMap(entityMetadata);

        entityDataService.registerService('Job', jobsDataService);

    }


}
