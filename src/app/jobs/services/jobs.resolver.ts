import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {JobEntityService} from './job-entity.service';
import {filter, first, map, tap} from 'rxjs/operators';


@Injectable()
export class JobsResolver implements Resolve<boolean> {

    constructor(private jobsService: JobEntityService) {

    }

    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<boolean> {

        return this.jobsService.loaded$
            .pipe(
                tap(loaded => {
                    if (!loaded) {
                       this.jobsService.getAll();
                    }
                }),
                filter(loaded => !!loaded),
                first()
            );

    }

}
