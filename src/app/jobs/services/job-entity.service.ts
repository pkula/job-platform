import {Injectable} from '@angular/core';
import {EntityCollectionServiceBase, EntityCollectionServiceElementsFactory} from '@ngrx/data';
import {Job} from '../model/job';


@Injectable()
export class JobEntityService
    extends EntityCollectionServiceBase<Job> {

    constructor(
        serviceElementsFactory:
            EntityCollectionServiceElementsFactory) {

        super('Job', serviceElementsFactory);

    }

}

