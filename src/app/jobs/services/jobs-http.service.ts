import {Injectable} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {Job} from "../model/job";
import {map} from "rxjs/operators";


@Injectable()
export class JobsHttpService {

    constructor(private http:HttpClient) {

    }

    findAllJobs(): Observable<Job[]> {
        return this.http.get('/api/jobs')
            .pipe(
                map(res => res['payload'])
            );
    }

    findJobByUrl(jobUrl: string): Observable<Job> {
      return this.http.get<Job>(`/api/jobs/${jobUrl}`);
    }



    saveJob(jobId: string | number, changes: Partial<Job>) {
        return this.http.put('/api/job/' + jobId, changes);
    }


}
