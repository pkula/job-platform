import {Injectable} from '@angular/core';
import {DefaultDataService, HttpUrlGenerator} from '@ngrx/data';
import {Job} from '../model/job';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';


@Injectable()
export class JobsDataService extends DefaultDataService<Job> {


  constructor(http: HttpClient, httpUrlGenerator: HttpUrlGenerator) {
    super('Job', http, httpUrlGenerator);

  }

  getAll(): Observable<Job[]> {
    return this.http.get('/api/jobs')
      .pipe(
        map(res => res['payload'])
      );
  }

  favouriteJob(job: Job): Observable<any> {
    return this.http.post('/api/job/' + job.id.toString() + '/favourite', {})
      .pipe(
        map(res => res['payload'])
      );
  }
  
  applyJob(job: Job): Observable<any> {
    return this.http.post('/api/job/' + job.id.toString() + '/apply', {})
      .pipe(
        map(res => res['payload'])
      );
  }
}
