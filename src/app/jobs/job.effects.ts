import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {map, switchMap, tap, withLatestFrom} from 'rxjs/operators';
import {Router} from '@angular/router';
import * as JobActions from './job.actions';
import {JobsDataService} from './services/jobs-data.service';
import {AuthService} from '../auth/auth.service';
import {reload} from '../auth/auth.actions';
import {Store} from '@ngrx/store';
import {loggedUser} from '../auth/auth.selectors';


@Injectable()
export class JobEffects {

  constructor(private actions$: Actions,
              private jobDataService: JobsDataService,
              private authService: AuthService,
              private store: Store,
              private router: Router) {

  }

  favourite$ = createEffect(() =>
      this.actions$
        .pipe(
          ofType(JobActions.favourite),
          withLatestFrom(this.store.select(loggedUser)),
          map(([action,userReadOnly]) => {
            let user = JSON.parse(JSON.stringify(userReadOnly));
            if (!user.favouriteJobs.includes(action.job.id)) {
              user.favouriteJobs.push(action.job.id);
            } else {
              user.favouriteJobs = user.favouriteJobs.filter(it => it != action.job.id);
            }
            return (reload({user: user}));
            }
          )
        )
    , {dispatch: true}
  );
  applyToJob$ = createEffect(() =>
      this.actions$
        .pipe(
          ofType(JobActions.applyToJob),
          withLatestFrom(this.store.select(loggedUser)),
          map(([action,userReadOnly]) => {
            let user = JSON.parse(JSON.stringify(userReadOnly));
            if (!user.appliedJobs.includes(action.job.id)) {
              user.appliedJobs.push(action.job.id);
            } else {
              user.appliedJobs = user.appliedJobs.filter(it => it != action.job.id);
            }
            return (reload({user: user}));
            }
          )
        )
    , {dispatch: true}
  );

}
