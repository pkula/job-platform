import {createAction, props} from '@ngrx/store';
import {Job} from './model/job';


export const favourite = createAction(
  "[Job Seeker] Favourite",
  props<{job: Job}>()
);
export const applyToJob = createAction(
  "[Job Seeker] Apply To Job",
  props<{job: Job}>()
);
