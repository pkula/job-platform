import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Job } from '../model/job';
import { Observable, Subject, Subscription } from 'rxjs';
import { defaultDialogConfig } from '../shared/default-dialog-config';
import { EditJobDialogComponent } from '../edit-job-dialog/edit-job-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { JobEntityService } from '../services/job-entity.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatChip } from '@angular/material/chips';
import { select, Store } from '@ngrx/store';
import { selectCategories } from '../../shared/state/selection.actions';
import { debounceTime, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { isLoggedIn, selectAuthState, loggedUser } from '../../auth/auth.selectors';


@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeComponent implements OnInit {


  jobs$: Observable<Job[]>;
  categoriesStatic = [
    { value: "Angular", selected: false },
    { value: "Java", selected: false },
    { value: "React", selected: false }

  ];
  showCategoriesFilter = false;
  showAddJob = false;
  showSearchBox = false;
  form: FormGroup;
  constructor(
    private dialog: MatDialog,
    private store: Store,
    private jobsService: JobEntityService) {

  }

  private searchModel: Subject<string> = new Subject<string>();
  private searchSubscription: Subscription;
  debounceTime = 500;


  searchFilter() {
    this.reload()
  }

  onSearchChange(searchValue : string ) {
    this.searchModel.next(searchValue)
  }

  ngOnDestroy(): void {
    this.searchSubscription.unsubscribe();
  }
  ngOnInit() {
    this.searchSubscription = this.searchModel
      .pipe(
        debounceTime(this.debounceTime),
      )
      .subscribe(() => {
        this.searchFilter();
      });
    this.store
      .pipe(
        select(loggedUser)
      ).subscribe(user => {

        this.showAddJob = user.role == 'recruiter';
        this.showSearchBox = user.role == 'recruiter' || user.role=='jobSeeker';
        this.showCategoriesFilter = user.role == 'jobSeeker';
      });
    this.reload();
  }


  toggleSelection(chip: MatChip) {
    this.categoriesStatic.find(it => it.value == chip.value).selected = !this.categoriesStatic.find(it => it.value == chip.value).selected;
    chip.toggleSelected();
    this.store.dispatch(selectCategories({ categories: this.categoriesStatic.filter(it => it.selected).map(it => it.value) }))
    this.reload()
  }

  reload() {
    const selectedCategories = this.categoriesStatic.filter(it => it.selected).map(it => it.value);
    this.jobs$ = this.jobsService.entities$.pipe(withLatestFrom(this.store.select(loggedUser)),
      map(([jobs, userReadOnly]) => {
        if (userReadOnly.role == 'recruiter') {
          return jobs.filter(it => it.createdBy == +userReadOnly.id)
        } else {
          return jobs
        }
      }));

    if (selectedCategories.length > 0) {
      this.jobs$ = this.jobs$.pipe(map(it => it.filter(it => selectedCategories.includes(it.category))))
    }
    this.searchModel.subscribe(data=>{
      if(data){
      this.jobs$ = this.jobs$.pipe(map(it => it.filter(it => it.description.toLowerCase().includes(data.toLowerCase()))))

      }
    })
  }

  initChips() {
    this.form = new FormGroup({

      categories: new FormControl([])
    });
  }

  onAddJob() {

    const dialogConfig = defaultDialogConfig();

    dialogConfig.data = {
      dialogTitle: "Create Job",
      mode: 'create'
    };

    this.dialog.open(EditJobDialogComponent, dialogConfig);

  }


}
