import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Store} from '@ngrx/store';
import {loggedUser} from '../auth/auth.selectors';
import {concatMap, map, switchMap} from 'rxjs/operators';


@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private store: Store) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.store.select(loggedUser).pipe(concatMap(user => {
      if(user){
        const tokenizedReq = req.clone({headers: req.headers.set('Authorization', user.id.toString())});
        return next.handle(tokenizedReq);
      }
      return next.handle(req);
    }));
  }
}
