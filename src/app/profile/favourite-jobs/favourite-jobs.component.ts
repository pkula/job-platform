import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { User } from '../../auth/model/user.model';
import { AppState } from '../../reducers';

import { loggedUser} from '../../auth/auth.selectors';
import { Job } from '../../jobs/model/job';
import { JobEntityService } from '../../jobs/services/job-entity.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'favourite-jobs',
  templateUrl: './favourite-jobs.component.html',
  styleUrls: ['./favourite-jobs.component.css']
})
export class FavouriteJobsComponent implements OnInit {

  loggedUser$: Observable<User>;//jobs

  favouriteJobs$:Observable<Job[]>

  constructor(
     
      private store: Store<AppState>,
      private jobsService: JobEntityService) {



  }

  ngOnInit(): void {
    this.loggedUser$ = this.store
            .pipe(
                select(loggedUser)
            );
            this.reload();
           
     
  }



  public reload() {
    this.loggedUser$.subscribe(user => {
      this.favouriteJobs$ = this.jobsService.entities$
        .pipe(
          map(jobs => jobs.filter(job => user.favouriteJobs.includes(job.id)))
        );

    });
  }
}
