import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from "@angular/material/card";
import { MatInputModule } from "@angular/material/input";
import { RouterModule } from "@angular/router";
import { ReactiveFormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";

import { MatRadioModule } from '@angular/material/radio';
import { MatGridListModule } from '@angular/material/grid-list';
import { ProfileComponent } from './profile.component';
import { AuthGuard } from '../auth/auth.guard';
import { MatTabsModule } from '@angular/material/tabs';
import { ProfileInfoComponent } from './profile-info/profile-info.component';
import { FavouriteJobsComponent } from './favourite-jobs/favourite-jobs.component';
import { AppliedJobsComponent } from './applied-jobs/applied-jobs.component';
import { JobsModule } from '../jobs/jobs.module';
import { SharedModule } from '../shared/shared.module';
import { EntityDefinitionService, EntityDataService, EntityMetadataMap } from '@ngrx/data';
import { compareJobs } from '../jobs/model/job';
import { JobsDataService } from '../jobs/services/jobs-data.service';
import { JobsResolver } from '../jobs/services/jobs.resolver';

const route = [
    {
        path: '', component: ProfileComponent,
        resolve: {
            jobs: JobsResolver
        }
    }

]
@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        MatCardModule,
        MatInputModule,
        MatButtonModule,
        MatRadioModule,
        MatGridListModule,
        MatTabsModule,
        RouterModule.forChild(route),
        SharedModule,

    ],
    declarations: [ProfileComponent, ProfileInfoComponent, FavouriteJobsComponent, AppliedJobsComponent],
    exports: [],
    providers: [JobsDataService,JobsResolver]
})
export class ProfileModule {
    static forRoot(): ModuleWithProviders<any> {
        return {
            ngModule: ProfileModule,
            providers: [
                AuthGuard
            ]
        }
    }

    constructor(
        private eds: EntityDefinitionService,
        private entityDataService: EntityDataService,
        private jobsDataService: JobsDataService) {

        eds.registerMetadataMap(entityMetadata);

        entityDataService.registerService('Job', jobsDataService);

    }

}
const entityMetadata: EntityMetadataMap = {
    Job: {
        sortComparer: compareJobs,
        entityDispatcherOptions: {
            optimisticUpdate: true
        }
    }
};