import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { User } from '../../auth/model/user.model';
import { AppState } from '../../reducers';
import { loggedUser} from '../../auth/auth.selectors';
import { UntypedFormGroup, UntypedFormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'profile-info',
  templateUrl: './profile-info.component.html',
  styleUrls: ['./profile-info.component.css']
})
export class ProfileInfoComponent implements OnInit {
  loggedUser$: Observable<User>;

  form: UntypedFormGroup;

  constructor(
      private fb:UntypedFormBuilder,
      private auth: AuthService,
      private router:Router,
      private store: Store<AppState>) {



  }

  ngOnInit(): void {
    this.loggedUser$ = this.store
            .pipe(
                select(loggedUser)
            );
      this.loggedUser$.subscribe(user =>{
        this.form = this.fb.group({
          firstName: [user.firstName, [Validators.required]],
          lastName: [user.lastName, [Validators.required]],
          email: [user.email, [Validators.required]],
          role: [user.role, [Validators.required]]
        });
      })
  }

}
