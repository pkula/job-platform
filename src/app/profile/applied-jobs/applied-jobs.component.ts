import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { loggedUser } from '../../auth/auth.selectors';
import { User } from '../../auth/model/user.model';
import { Job } from '../../jobs/model/job';
import { JobEntityService } from '../../jobs/services/job-entity.service';
import { AppState } from '../../reducers';

@Component({
  selector: 'applied-jobs',
  templateUrl: './applied-jobs.component.html',
  styleUrls: ['./applied-jobs.component.css']
})

export class AppliedJobsComponent implements OnInit {

  loggedUser$: Observable<User>;//jobs

  appliedJobs$:Observable<Job[]>

  constructor( private store: Store<AppState>,
               private jobsService: JobEntityService) { }

  ngOnInit(): void {
    this.loggedUser$ = this.store
            .pipe(
                select(loggedUser)
            );
            this.reload();
  }

  public reload() {
    this.loggedUser$.subscribe(user => {
      this.appliedJobs$ = this.jobsService.entities$
        .pipe(
          map(jobs => jobs.filter(job => user.appliedJobs.includes(job.id)))
        );

    });
  }

}
