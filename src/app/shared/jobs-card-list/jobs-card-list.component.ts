import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Job} from '../../jobs/model/job';
import {MatDialog} from '@angular/material/dialog';
import {EditJobDialogComponent} from '../../jobs/edit-job-dialog/edit-job-dialog.component';
import {defaultDialogConfig} from '../../jobs/shared/default-dialog-config';
import {JobEntityService} from '../../jobs/services/job-entity.service';
import {Router} from '@angular/router';
import {select, Store} from '@ngrx/store';
import {loggedUser} from '../../auth/auth.selectors';
import {Observable, of} from 'rxjs';
import {User} from '../../auth/model/user.model';
import {AppState} from '../../reducers';
import {reload} from '../../auth/auth.actions';
import {JobsDataService} from '../../jobs/services/jobs-data.service';
import {AuthService} from '../../auth/auth.service';
import {catchError, take, tap} from 'rxjs/operators';
import {applyToJob, favourite} from '../../jobs/job.actions';

@Component({
  selector: 'jobs-card-list',
  templateUrl: './jobs-card-list.component.html',
  styleUrls: ['./jobs-card-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class JobsCardListComponent implements OnInit {

  @Input()
  jobs: Job[];

  @Output()
  jobChanged = new EventEmitter();
  public loggedUser$: Observable<User>;

  public showEditJob = false;
  public showApplyJob = false;
  public showAddFavourite = false;
  public showDeleteJob = false;

  constructor(
    private dialog: MatDialog,
    private jobService: JobEntityService,
    private authService: AuthService,
    private router: Router,
    private jobDataService: JobsDataService,
    private store: Store<AppState>) {
  }

  ngOnInit() {
    this.loggedUser$ = this.store
      .pipe(
        select(loggedUser)
      );
    this.loggedUser$.subscribe(user => {
      this.showAddFavourite = user.role == 'jobSeeker';
      this.showApplyJob = user.role == 'jobSeeker';
      this.showEditJob = user.role == 'recruiter';
      this.showDeleteJob = user.role == 'recruiter';
    });
  }

  editJob(job: Job) {

    const dialogConfig = defaultDialogConfig();

    dialogConfig.data = {
      dialogTitle: 'Edit Job',
      job,
      mode: 'update'
    };

    this.dialog.open(EditJobDialogComponent, dialogConfig)
      .afterClosed()
      .subscribe(() => this.jobChanged.emit());

  }

  viewJob(job: Job) {
    this.router.navigateByUrl('/jobs/' + job.url);
  }

  onDeleteJob(job: Job) {
    this.jobService.delete(job)
      .subscribe(
        () => console.log('Delete completed'),
        err => console.log('Deleted failed', err)
      );
  }

  toggleFavouriteJob(job: Job) {

    this.store.dispatch(favourite({job:job}));
    this.jobDataService.favouriteJob(job)
      .pipe(
        catchError(
          err => of(this.store.dispatch(favourite({job:job}))
          ),
        ),
      )
      .subscribe(data=>{
      console.log(data)
    },()=>{
    });
  }

  applyJob(job: Job) {
    this.store.dispatch(applyToJob({job:job}));
    this.jobDataService.applyJob(job)
      .pipe(
        catchError(
          err => of(this.store.dispatch(applyToJob({job:job}))
          ),
        ),
      )
      .subscribe(data=>{
      console.log(data)
    },()=>{
    });
  }
}









