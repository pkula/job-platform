import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JobsCardListComponent } from './jobs-card-list/jobs-card-list.component';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import { MatTabsModule } from '@angular/material/tabs';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { JobEntityService } from '../jobs/services/job-entity.service';
import { EntityDefinitionService, EntityDataService, EntityMetadataMap } from '@ngrx/data';
import { JobsDataService } from '../jobs/services/jobs-data.service';
import { compareJobs } from '../jobs/model/job';
import {EffectsModule} from '@ngrx/effects';
import {JobEffects} from '../jobs/job.effects';
import { StoreModule } from '@ngrx/store';
import { appStateReducer } from './state/reducers';



@NgModule({
  declarations: [JobsCardListComponent],
  imports: [

    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatTabsModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatSlideToggleModule,
    MatDialogModule,
    MatSelectModule,
    MatDatepickerModule,
    MatMomentDateModule,
    FlexLayoutModule,
    CommonModule,
    StoreModule.forFeature('app', appStateReducer),
    EffectsModule.forFeature([JobEffects])
  ],
  exports: [
    JobsCardListComponent
  ],
  providers: [
    JobEntityService,
  ]

})
export class SharedModule {

};
