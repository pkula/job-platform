import {createAction, props} from '@ngrx/store';


export const selectCategories = createAction(
    "[Home Page] Select Categories",
    props<{categories: string[]}>()
);


