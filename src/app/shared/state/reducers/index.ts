import {
    ActionReducer,
    ActionReducerMap,
    createFeatureSelector, createReducer,
    createSelector,
    MetaReducer, on
} from '@ngrx/store';
import { SelectionActions } from '../action-types';

export interface AppState {
    categories: string[]
}

export const initialAuthState: AppState = {
    categories: undefined
};

export const appStateReducer = createReducer(

    initialAuthState,

    on(SelectionActions.selectCategories, (state, action) => {
        return {
            categories: action.categories
        }
    }),



);
