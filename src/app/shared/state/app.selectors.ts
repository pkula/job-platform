import {createFeatureSelector, createSelector} from '@ngrx/store';
import { AppState } from './reducers';

export const selectAppState =
createFeatureSelector<AppState>("app");

export const selectedCategories = createSelector(selectAppState,app=>app.categories)

