import {Request, Response} from 'express';
import {JOBS} from "./db-data";


export function deleteJob(req: Request, res: Response) {

    console.log("Deleting job ...");

    const id = req.params["id"];

    const job = JOBS[id];

    delete JOBS[id];

    setTimeout(() => {

      res.status(200).json({id});

    }, 2000);

}

