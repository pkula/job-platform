

import * as express from 'express';
import {Application} from "express";
import {getAllJobs, getJobByUrl} from "./get-jobs.route";
import {loginUser, registerUser} from "./auth.route";
import {saveJob} from "./save-job.route";
import {createJob} from './create-job.route';
import {deleteJob} from './delete-job.route';
import {favouriteJob} from './favourite-jobs.route';
import {getMe} from './get-logged-user.route';
import { applyJob } from './apply-job.route';

const bodyParser = require('body-parser');



const app: Application = express();

const cors = require('cors');

app.use(cors({origin: true}));

app.use(bodyParser.json());


app.route('/api/login').post(loginUser);

app.route('/api/register').post(registerUser);

app.route('/api/jobs').get(getAllJobs);

app.route('/api/job').post(createJob);

app.route('/api/job/:id').put(saveJob);

app.route('/api/job/:id').delete(deleteJob);

app.route('/api/job/:id/favourite').post(favouriteJob);

app.route('/api/job/:id/apply').post(applyJob);

app.route('/api/jobs/:jobUrl').get(getJobByUrl);

app.route('/api/me').get(getMe);




const httpServer:any = app.listen(9000, () => {
    console.log("HTTP REST API Server running at http://localhost:" + httpServer.address().port);
});




