

import {Request, Response} from 'express';
import {JOBS} from "./db-data";



export function getAllJobs(req: Request, res: Response) {

    console.log("Retrieving jobs data ...");

    setTimeout(() => {

      res.status(200).json({payload:Object.values(JOBS)});

    }, 1000);



}


export function getJobByUrl(req: Request, res: Response) {

    const jobUrl = req.params["jobUrl"];

    const jobs:any = Object.values(JOBS);

    const job =jobs.find(job => job.url == jobUrl);

    setTimeout(() => {

      res.status(200).json(job);

    }, 1000);


}
