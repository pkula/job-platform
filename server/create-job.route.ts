import {Request, Response} from 'express';
import {JOBS} from './db-data';

export var jobsKeyCounter = 100;

export function createJob(req: Request, res: Response) {

    console.log("Creating new job ...");

    const changes = req.body;

    const userId = req.header("Authorization").toString()
    const newJob = {
        id: jobsKeyCounter,
      seqNo: jobsKeyCounter,
      createdBy: +userId,
        ...changes
    };

  JOBS[newJob.id] = newJob;
  console.log(newJob)  
  jobsKeyCounter += 1;

    setTimeout(() => {

      res.status(200).json(newJob);

    }, 2000);

}

