import {Request, Response} from 'express';
import {JOBS, USERS} from './db-data';

export function applyJob(req: Request, res: Response) {

  console.log("Apply job ...");

  const id = req.params["id"];
  const userId = req.header("Authorization").toString()
  if(USERS[userId].appliedJobs.includes((+id))){
    USERS[userId].appliedJobs = USERS[userId].appliedJobs.filter(it=>it!=(+id))
    JOBS[id].applicants = JOBS[id].applicants.filter(it=>it.id!=(+userId))
  }else{
    USERS[userId].appliedJobs.push(+id)
    JOBS[id].applicants.push(USERS[+userId])
  }
  setTimeout(() => {

    res.status(200).json({id});

  }, 2000);

}
