export const USERS = {
  1: {
    id: 1,
    email: 'jobSeeker',
    password: 'test',
    firstName: 'jobSeeker',
    lastName: 'test',
    role: 'jobSeeker',
    favouriteJobs: [
      1, 3
    ],
    appliedJobs: [
      4, 2
    ]
  },
  2: {
    id: 2,
    email: 'recruiter',
    password: 'test',
    firstName: 'recruiter',
    lastName: 'test',
    role: 'recruiter',
    favouriteJobs: [
    ],
    appliedJobs: [
    ],
    createdJobs:[
      1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16
    ]
  },

};


export const JOBS: any = {

  4: {
    id: 4,
    description: 'Angular Developer',
    longDescription: 'Angular Developer With NgRx Experience',
    iconUrl: 'https://www.cci-summit.de/fileadmin/user_upload/automotiveit-kongress/aITK2020/partner/LHIS_App_neu.jpg',
    category: 'Angular',
    seqNo: 0,
    createdBy: 2, applicants: [{
      id: 1,
      email: 'jobSeeker',
      password: 'test',
      firstName: 'jobSeeker',
      lastName: 'test',
      role: 'jobSeeker',
      favouriteJobs: [
        4, 2
      ],
      appliedJobs: [
        3, 1, 6, 7, 12
      ]
    }],
    url: 'ngrx-job'
  },

  2: {
    id: 2,
    description: 'Angular Core Deep Dive',
    longDescription: 'A detailed walk-through of the most important part of Angular - the Core and Common modules',
    iconUrl: 'https://www.cci-summit.de/fileadmin/user_upload/automotiveit-kongress/aITK2020/partner/LHIS_App_neu.jpg',
    category: 'Angular',
    seqNo: 1, createdBy: 2, applicants: [{
      id: 1,
      email: 'jobSeeker',
      password: 'test',
      firstName: 'jobSeeker',
      lastName: 'test',
      role: 'jobSeeker',
      favouriteJobs: [
        4, 2
      ],
      appliedJobs: [
        3, 1, 6, 7, 12
      ]
    }],
    url: 'angular-core-job'
  },

  3: {
    id: 3,
    description: 'RxJs In Practice Job',
    longDescription: 'Understand the RxJs Observable pattern, learn the RxJs Operators via practical examples',
    iconUrl: 'https://www.cci-summit.de/fileadmin/user_upload/automotiveit-kongress/aITK2020/partner/LHIS_App_neu.jpg',
    category: 'Angular',
    seqNo: 2, createdBy: 2, applicants: [],
    url: 'rxjs-job'
  },

  1: {
    id: 1,
    description: 'Serverless Angular with Firebase Job',
    longDescription: 'Serveless Angular with Firestore, Firebase Storage & Hosting, Firebase Cloud Functions & AngularFire',
    iconUrl: 'https://www.cci-summit.de/fileadmin/user_upload/automotiveit-kongress/aITK2020/partner/LHIS_App_neu.jpg',
    category: 'Angular',
    seqNo: 4, createdBy: 2, applicants: [],
    url: 'serverless-angular'
  },


  5: {
    id: 5,
    description: 'Angular for Angulars',
    longDescription: 'Establish a solid layer of fundamentals, learn what\'s under the hood of Angular',
    iconUrl: 'https://www.cci-summit.de/fileadmin/user_upload/automotiveit-kongress/aITK2020/partner/LHIS_App_neu.jpg',
    category: 'Angular',
    seqNo: 5, createdBy: 2, applicants: [],
    url: 'angular-for-Angulars'
  },


  12: {
    id: 12,
    description: 'Angular Testing Job',
    longDescription: 'In-depth guide to Unit Testing and E2E Testing of Angular Applications',
    iconUrl: 'https://www.cci-summit.de/fileadmin/user_upload/automotiveit-kongress/aITK2020/partner/LHIS_App_neu.jpg',
    category: 'Angular',
    seqNo: 6, createdBy: 2, applicants: [],
    url: 'angular-testing-job',
  },

  6: {
    id: 6,
    description: 'Angular Security Job - Web Security Fundamentals',
    longDescription: 'Learn Web Security Fundamentals and apply them to defend an Angular / Node Application from multiple types of attacks.',
    iconUrl: 'https://www.cci-summit.de/fileadmin/user_upload/automotiveit-kongress/aITK2020/partner/LHIS_App_neu.jpg',
    category: 'Angular',
    seqNo: 7, createdBy: 2, applicants: [],
    url: 'angular-security-job'
  },

  7: {
    id: 7,
    description: 'Angular PWA - Progressive Web Apps Job',
    longDescription: 'Learn Angular Progressive Web Applications, build the future of the Web Today.',
    iconUrl: 'https://www.cci-summit.de/fileadmin/user_upload/automotiveit-kongress/aITK2020/partner/LHIS_App_neu.jpg',
    category: 'Angular',
    seqNo: 8, createdBy: 2, applicants: [],
    url: 'angular-pwa-job'
  },

  8: {
    id: 8,
    description: 'Angular  Library Laboratory: Build Your Own Library',
    longDescription: 'Learn  Angular functionality typically used in Library Development. Angular Components, Directives, Testing, Npm',
    iconUrl: 'https://www.cci-summit.de/fileadmin/user_upload/automotiveit-kongress/aITK2020/partner/LHIS_App_neu.jpg',
    category: 'Angular',
    seqNo: 9, createdBy: 2, applicants: [],
    url: 'angular-Java-job'
  },

  9: {
    id: 9,
    description: 'The Complete Typescript Job',
    longDescription: 'Complete Guide to Typescript From Scratch: Learn the language in-depth and use it to build a Node REST API.',
    iconUrl: 'https://www.cci-summit.de/fileadmin/user_upload/automotiveit-kongress/aITK2020/partner/LHIS_App_neu.jpg',
    category: 'Angular',
    seqNo: 10, createdBy: 2, applicants: [],
    url: 'typescript-job'
  },

  10: {
    id: 10,
    description: 'Rxjs and Reactive Patterns Angular Architecture Job',
    longDescription: 'Learn the core RxJs Observable Pattern as well and many other Design Patterns for building Reactive Angular Applications.',
    iconUrl: 'https://www.cci-summit.de/fileadmin/user_upload/automotiveit-kongress/aITK2020/partner/LHIS_App_neu.jpg',
    category: 'Angular',
    seqNo: 11, createdBy: 2, applicants: [],
    url: 'rxjs-patterns-job'
  },

  11: {
    id: 11,
    description: 'Angular Material Job',
    longDescription: 'Build Applications with the official Angular Widget Library',
    iconUrl: 'https://www.cci-summit.de/fileadmin/user_upload/automotiveit-kongress/aITK2020/partner/LHIS_App_neu.jpg',
    category: 'Angular',
    seqNo: 12, createdBy: 2, applicants: [],
    url: 'angular-material-job'
  },
  13: {
    id: 13,
    description: 'Java Developer',
    longDescription: 'Java Developer With three years Experience',
    iconUrl: 'https://www.cci-summit.de/fileadmin/user_upload/automotiveit-kongress/aITK2020/partner/LHIS_App_neu.jpg',
    category: 'Java',
    seqNo: 0,
    createdBy: 2, applicants: [{
      id: 1,
      email: 'jobSeeker',
      password: 'test',
      firstName: 'jobSeeker',
      lastName: 'test',
      role: 'jobSeeker',
      favouriteJobs: [
        4, 2
      ],
      appliedJobs: [
        3, 1, 6, 7, 12
      ]
    }],
    url: 'java-job'
  },
  14: {
    id: 14,
    description: 'React Developer',
    longDescription: 'React Developer With three years Experience',
    iconUrl: 'https://www.cci-summit.de/fileadmin/user_upload/automotiveit-kongress/aITK2020/partner/LHIS_App_neu.jpg',
    category: 'React',
    seqNo: 0,
    createdBy: 2, applicants: [{
      id: 1,
      email: 'jobSeeker',
      password: 'test',
      firstName: 'jobSeeker',
      lastName: 'test',
      role: 'jobSeeker',
      favouriteJobs: [
        4, 2
      ],
      appliedJobs: [
        3, 1, 6, 7, 12
      ]
    }],
    url: 'java-job'
  },
  15: {
    id: 15,
    description: 'React Developer',
    longDescription: 'Junior React Developer',
    iconUrl: 'https://www.cci-summit.de/fileadmin/user_upload/automotiveit-kongress/aITK2020/partner/LHIS_App_neu.jpg',
    category: 'React',
    seqNo: 0,
    createdBy: 2, applicants: [{
      id: 1,
      email: 'jobSeeker',
      password: 'test',
      firstName: 'jobSeeker',
      lastName: 'test',
      role: 'jobSeeker',
      favouriteJobs: [
        4, 2
      ],
      appliedJobs: [
        3, 1, 6, 7, 12
      ]
    }],
    url: 'java-job'
  },
  16: {
    id: 16,
    description: 'Java Developer',
    longDescription: 'Junior Java Developer',
    iconUrl: 'https://www.cci-summit.de/fileadmin/user_upload/automotiveit-kongress/aITK2020/partner/LHIS_App_neu.jpg',
    category: 'Java',
    seqNo: 0,
    createdBy: 2, applicants: [{
      id: 1,
      email: 'jobSeeker',
      password: 'test',
      firstName: 'jobSeeker',
      lastName: 'test',
      role: 'jobSeeker',
      favouriteJobs: [
        4, 2
      ],
      appliedJobs: [
        3, 1, 6, 7, 12
      ]
    }],
    url: 'java-job'
  },
  

};





export function findJobById(jobId: number) {
  return JOBS[jobId];
}



export function authenticate(email: string, password: string) {

  const user: any = Object.values(USERS).find(user => user.email === email);

  if (user && user.password == password) {
    return user;
  } else {
    return undefined;
  }


}

export function register(user: any) {

  const u: any = Object.values(USERS).find(u => u.email === user.email);

  if (u) {
    return undefined;
  } else {
    const id = Object.values(USERS).length + 1;
    user['id'] = id;
    return USERS[id] = user;
  }

}

