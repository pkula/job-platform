import {Request, Response} from 'express';
import {USERS} from './db-data';

export function getMe(req: Request, res: Response) {

  console.log("Get me ...");

  const userId = req.header("Authorization").toString()

  setTimeout(() => {

    res.status(200).json(USERS[userId]);

  }, 2000);

}
