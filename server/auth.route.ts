

import {Request, Response} from 'express';
import {authenticate, register} from "./db-data";




export function loginUser(req: Request, res: Response) {

    console.log("User login attempt ...");

    const {email, password} = req.body;

    const user = authenticate(email, password);

    if (user) {
        res.status(200).json(user);
    }
    else {
        res.sendStatus(403);
    }

}
export function registerUser(req: Request, res: Response) {

    console.log("User register attempt ...");

    const body = req.body;

    const user = register(body);

    if (user) {
        res.status(200).json(body);
    }
    else {
        res.sendStatus(403);
    }

}



