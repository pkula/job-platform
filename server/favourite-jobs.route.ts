import {Request, Response} from 'express';
import {JOBS, USERS} from './db-data';

export function favouriteJob(req: Request, res: Response) {

  console.log("Favourite job ...");

  const id = req.params["id"];
  const userId = req.header("Authorization").toString()
  if(USERS[userId].favouriteJobs.includes((+id))){
    USERS[userId].favouriteJobs = USERS[userId].favouriteJobs.filter(it=>it!=(+id))
  }else{
    USERS[userId].favouriteJobs.push(+id)
  }
  setTimeout(() => {

    res.status(200).json({id});

  }, 2000);

}
