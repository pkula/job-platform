import {Request, Response} from 'express';
import {JOBS} from "./db-data";


export function saveJob(req: Request, res: Response) {

    console.log("Saving job ...");

    const id = req.params["id"],
        changes = req.body;

    JOBS[id] = {
        ...JOBS[id],
        ...changes
    };

    setTimeout(() => {

      res.status(200).json(JOBS[id]);

    }, 2000);

}

